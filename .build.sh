#!/bin/bash
wget https://github.com/mozilla/pdf.js/archive/gh-pages.zip
unzip gh-pages.zip
mkdir -p public/
mv -v pdf.js-gh-pages/* public/
rm -v gh-pages.zip
rmdir pdf.js-gh-pages
mkdir pdfsizeopt
cd pdfsizeopt
wget -O pdfsizeopt_libexec_linux.tar.gz https://github.com/pts/pdfsizeopt/releases/download/2017-01-24/pdfsizeopt_libexec_linux-v3.tar.gz
tar -xzvf pdfsizeopt_libexec_linux.tar.gz
#rm -fv pdfsizeopt_libexec_linux.tar.gz
wget -O pdfsizeopt.single https://raw.githubusercontent.com/pts/pdfsizeopt/master/pdfsizeopt.single
chmod +x pdfsizeopt.single
ln -s pdfsizeopt.single pdfsizeopt
cd ..
ORIG_PDF=$(ls *.pdf)
ARCHIVE=`date '+$ORIG_PDF_%Y-%m-%d_%H:%M'`
COMP_PDF="compressed_$ORIG_PDF"
pdfsizeopt/pdfsizeopt $ORIG_PDF $COMP_PDF
ls -lh $ORIG_PDF
ls -lh $COMP_PDF
mv $COMP_PDF public/
echo "File $ORIG_PDF compressed"
cd public
COMP_PDF=$(ls *.pdf |grep ^"compressed")
grep -lr compressed.tracemonkey-pldi-09.pdf . |xargs sed -i -e "s/compressed.tracemonkey-pldi-09.pdf/compressed_Aequatoria_Philharmonia.pdf/g" 
sed -i 's|src="../|src="|g' web/viewer.html
grep -lr "'../" web | xargs sed -i -e "s|'../|'|g"
rsync -avh web/ .
echo $COMP_PDF
cp -v viewer.html index.html
cd ..
python -m http.server 8000
